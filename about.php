<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="copyright" lang="en" content="Webdesign: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<script type='text/javascript' src="js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
    <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    	$.localScroll();
    });
    </script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - About Us </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
	<div id="promotions">
		<?php include 'promotions.php';?>
	</div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/promo2.jpg" alt="" /></li>
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->
    
    <h1 id="title">About Us</h1>
    
    <!-- Content Rows -->
    <div class="rows separation">    
        <div class="rows-content box separator" id="mission">
	    	<div class="row">            
	            <img src="images/mission.jpg"/>
	            <h2><span class="distribution">Our Mission</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>To supply tubular solutions such as piping, valves, fittings and process skid mounted equipment for the Oil, Gas and Energy industries. We are committed to servicing our clients in a professional manner with the highest quality standard. To achieve this goal, we combine our engineering capacity to supply specialized components in Latin American and Caribean market.</p>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="vision">
	    	<div class="row">            
	            <img src="images/vision.jpg"/>
	            <h2><span class="distribution">Our Vision</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>To be in the next 5 years the leading providers of pipe, valves and fittings, as well as skid mounted process equipment. We aim to establish ourselves as a trusted source for special fittings and process pipe, with our own assembly operation and inventory. To be widely recognized in the Oil & Gas industry in Latin America and the Caribbean, supported by the experience of our staff and efficiency of our processes, working with the highest quality standards to meet the particular needs of our clients.</p>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="quality-policy">
	    	<div class="row">            
	            <img src="images/quality.jpg"/>
	            <h2><span class="distribution">Quality Policy</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>TEKNACORP is fully committed to continually improve and maintain the Quality Management System, through the effective implementation of ISO and API standard. Our Policy is a framework for defining quality objectives established by the organization. Our main goal is to satisfy the needs, requirements and expectations of our customers in Latin America and the Caribbean through excellence in the provision of pipes, valves and fittings. We add the value of logistics and engineering through the joint effort of our team and our suppliers.</p>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="people">
	    	<div class="row">            
	            <img src="images/people.jpg"/>
	            <h2><span class="distribution">Our People</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>We are a consolidated team of engineers, technicians and administrative support staff with an extensive experience in the Oil & Gas Industry. Each one of us is committed to evaluate your requirements in order to produce the fastest and most cost effective solution available in the market.</p>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="values">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/values.jpg"/>
	            <h2><span class="distribution">Our Values</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul>
	                <li>Quality</li>
	                <li>Speed</li>
	                <li>Professionalism</li>
	                <li>Accesibility</li>
	                <li>Clear Communication</li>
	                <li>Knowledge</li>
	                <li>Integrity</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
    </div> <!-- /rows -->

    <!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->
</body>
</html>
