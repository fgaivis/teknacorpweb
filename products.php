<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="copyright" lang="en" content="Webdesign: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/nyroModal.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<!-- <script type='text/javascript' src="js/jquery.scrollTo-min.js"></script> -->
	<!-- <script type="text/javascript" src="js/jquery.localscroll-min.js"></script> -->
    <!-- <script type="text/javascript" src="js/jquery.simplemodal.js"></script> -->
    <script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>    
    <script type="text/javascript" src="js/jquery.nyroModal.custom.js"></script>
    <!--[if IE 6]>
		<script type="text/javascript" src="js/jquery.nyroModal-ie6.min.js"></script>
	<![endif]-->
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    //$(document).ready(function(){
    	//$.localScroll();
    //});
    </script>
    <script type="text/javascript">
		$(function() {
		  $('.nyroModal').nyroModal();
		});
	</script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - Products </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
	<div id="promotions">
		<?php include 'promotions.php';?>
	</div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/p_products.jpg" alt="" /></li>
                <!-- <li><img src="images/promo2.jpg" alt="" /></li>  -->
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->
    
    <h1 id="title">Products</h1>
    
    <!-- Content Rows -->
    <div class="rows separation">    
        <div class="rows-content box separator" id="pipes">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/process-pipe.jpg"/>
	            <h2><span class="products">Pipe</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li><a href="media/docs/2014/1_Line_pipe_as_per_API_5L.pdf">Line pipe as per API 5L</a></li>
	                <li><a href="media/docs/2014/2_Process_Pipe_as_per_ASTM.pdf">Process Pipe as per ASTM</a></li>
	                <li><a href="media/docs/2014/3_Carbon_Chrome_and_Stainless.pdf">Carbon, Chrome and Stainless</a></li>
	                <li><a href="media/docs/2014/4_Structural_Pipe_API_2H.pdf">Structural Pipe API 2H</a></li>
	                <li><a href="media/docs/2014/5_Casing_API_5CT.pdf">Casing Pipe API 5CT</a></li>
	                <li><a href="media/docs/2014/6_TUBING_SIZES_API_5CT.pdf">Tubing Sizes API 5CT</a></li>
	                <li><a href="media/docs/2014/7_API_5DP_DRILL_PIPE_SIZES.pdf">API 5DP Drill Pipe Sizes</a></li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="bends">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/bends.jpg"/>
	            <h2><span class="products">Bends</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Diameters from 1/2" to 66"</li>
	                <li>Short and Long radius, 3D &amp; 5D</li>
	                <li>Carbon Steels, Stainless, Diverse Alloys</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="fittings">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/fittings.jpg"/>
	            <h2><span class="products">Fittings</span></h2>            
	        </div> <!-- /row -->	        
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Butt Weld</li>
	                <li>Forged Steel</li>
	                <li><a href="images/products/Special_Tees.jpg" class="nyroModal" title="Special_Tee's" rel="gal">Special Tee's</a></li>
	                <li><a href="images/products/Split_Tee145.JPG" class="nyroModal" title="Split_Tee-145" rel="gal">Split tee Plugged Tee</a></li>
	                <li><a href="images/products/RADIAL_FLOW_TEE_opt.jpg" class="nyroModal" title="Radial_Flow_Tee" rel="gal">Radial Flow Tees</a></li>
	                <li>Barred Tee</li>
	                <li><a href="images/products/Special_Extruded_Headers.jpg" class="nyroModal" title="Special_Extruded_Headers" rel="gal">Special Extruded Headers</a></li>
	            </ul>
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="flanges">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/flanges.jpg"/>
	            <h2><span class="products">Flanges</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Blind Flanges</li>
	                <li>Slip On</li>
	                <li>Lap Joint</li>
	                <li>Threaded</li>
	                <li>Welding Neck</li>
	                <li>Figure "8"</li>
	            </ul>      
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="swivel-flanges">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/swivel-flanges.jpg"/>
	            <h2><span class="products">Swivel Flanges</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li><a href="#swivel-video" class="nyroModal">Swivel Flanges (Video)</a></li>
	                <li>Misalignment Flanges</li>
            	</ul>
            	<div id="swivel-video" style="display: none; width: 640px;">
				  <video width="640" height="480" controls>
				  	<source src="media/videos/swivel-flange.mp4" type="video/mp4">
					Your browser does not support the video tag.
				  </video>
				</div>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="cladding">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/cladding.jpg"/>
	            <h2><span class="products">Cladding</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Inlays / Overlays</li>
	                <li>Roll Welding</li>
	                <li>Explosion Welding</li>
	                <li><a href="images/products/cladding_del_weld_overlay.png" class="nyroModal" title="Cladding" rel="claddinggal">Cladding</a></li>
	                <li><a href="media/docs/CLAD_MANUFACTURING_PROCESS.pdf">Clad Manufacturing Process (PDF)</a></li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="strainers">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/filters.jpg"/>
	            <h2><span class="products">Strainers</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Basket Strainers</li>
	                <li>Temporary Strainers</li>
	                <li>"Y" Type Strainers</li>
	                <li>"T" Type Strainers</li>
	                <li>Miscellaneous</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="closures">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/quick-opening.jpeg"/>
	            <h2><span class="products">Closures</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Quick Opening Closures</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="valves">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/valves.jpg"/>
	            <h2><span class="products">Valves</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Gate, Globe, Check</li>
	                <li>Ball Valves</li>
	                <li>Forged Gate, Globe, Piston, Swing & Ball Check Valves</li>
	                <li><a href="images/products/actuated_ball_valve.jpg" class="nyroModal" title="Actuated_Ball_Valve" rel="valvegal">Actuated Valves</a></li>
	                <li>Cast Iron Valves</li>
	                <li>Butterfly Valves</li>
	                <li>Bronze Valves</li>
	                <li>Wafer Check Valves</li>
	                <li>Plug Valves</li>
	                <li><a href="images/products/valves/8_ACTUATOR_1.png" class="nyroModal" title="8_ACTUATOR_1" rel="actgal">8 '' 300# Trunnion Mounted Ball Valve (IMG 1)</a></li>
	                <li><a href="images/products/valves/8_ACTUATOR_2.png" class="nyroModal" title="8_ACTUATOR_2" rel="actgal">8 '' 300# Trunnion Mounted Ball Valve (IMG 2)</a></li>
	                <li><a href="images/products/valves/12_ACTUATOR_1.jpg" class="nyroModal" title="12_ACTUATOR_1" rel="actgal">12'' 900# Trunnion Mounted Ball Valve (IMG 1)</a></li>
	                <li><a href="images/products/valves/12_ACTUATOR_2.jpg" class="nyroModal" title="12_ACTUATOR_2" rel="actgal">12'' 900# Trunnion Mounted Ball Valve (IMG 2)</a></li>
	                <li><a href="images/products/valves/12_ACTUATOR_3.jpg" class="nyroModal" title="12_ACTUATOR_3" rel="actgal">12'' 900# Trunnion Mounted Ball Valve (IMG 3)</a></li>
	                <li><a href="images/products/valves/12_ACTUATOR_4.jpg" class="nyroModal" title="12_ACTUATOR_4" rel="actgal">12'' 900# Trunnion Mounted Ball Valve (IMG 4)</a></li>
	                <!-- <li><a href="media/docs/Trunnion_Mounted_Ball_Valve.pdf">Trunnion Mounted Ball Valve (PDF)</a></li> -->
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="coatings">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/coatings_fbe.jpg"/>
	            <h2><span class="products">Coatings</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Epoxy</li>
	                <li>Fusion Bonded Epoxy (FBE)</li>
	                <li>Polyurethane</li>
	                <li>Internal &amp; External</li>
	                <li>Special coatings</li>
	                <li>Buffing</li>
	                <li>Three Layer Polyethylene (3LPE)</li>
	                <li>Three Layer Polypropylene (3LPP)</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="pig-launchers">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/pig_launcher.jpg"/>
	            <h2><span class="products">Pig Launchers and Receivers</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">
	        	<p>All units are design and fabricated to required pipeline and vessel design codes including:</p>           
	            <ul class="links-list">
	                <li>ASME Stamp Division 1 & 2</li>
	                <li>ASME VIII</li>
	                <li>ASME B31.3, B31.4, B31.8</li>
	                <li><a href="images/products/PIG_LAUNCH_01.png" class="nyroModal" title="Pig_Launchers_1" rel="piggal">Pig Launchers (IMG 1)</a></li>
	                <li><a href="images/products/PIG_LAUNCH_02.jpg" class="nyroModal" title="Pig_Launchers_2" rel="piggal">Pig Launchers (IMG 2)</a></li>
	                <li><a href="images/products/PIG_LAUNCH_03.png" class="nyroModal" title="Pig_Launchers_3" rel="piggal">Pig Launchers (IMG 3)</a></li>
	                <li><a href="images/products/PIG_LAUNCH_04.jpg" class="nyroModal" title="Pig_Launchers_4" rel="piggal">Pig Launchers (IMG 4)</a></li>
	                <li><a href="images/products/PIG_LAUNCH_05.jpg" class="nyroModal" title="Pig_Launchers_5" rel="piggal">Pig Launchers (IMG 5)</a></li>
	                <li><a href="images/products/PIG_LAUNCH_06.jpg" class="nyroModal" title="Pig_Launchers_6" rel="piggal">Pig Launchers (IMG 6)</a></li>
	                <li><a href="images/products/PIG_LAUNCH_07.jpg" class="nyroModal" title="Pig_Launchers_7" rel="piggal">Pig Launchers (IMG 7)</a></li>
	                <!-- <li><a href="media/docs/2014/PIG_LAUNCHERS_RECEIVERS.pdf">Pig Launchers and Receivers</a></li> -->
            	</ul>       
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="im-joints">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/monoblock.jpg"/>
	            <h2><span class="products">Insulated Monolithic Joints</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <!-- <li><a href="media/docs/2014/Insulated_Monolithic_Joints.pdf">Insulated Monolithic Joints</a></li> -->
	                <li>Diameter Range: 4” o 42”</li>
	                <li>Classes: 150#, 300#, 600# and 900#</li>
	                <li>Codes and Standards: ASME VIII Div I, ASME IX, ASME B31.8 B31.4, B31.3 NACE MR0175, UNI 11105</li>
	                <li>Certification: ASME U-STAMP</li>
	                <li>Test Certification: BS EN 10204 3.1</li>
	                <li><a href="images/products/Ins_Mont Joints_001.jpg" class="nyroModal" title="Insulated_Monolithic_Joints_1" rel="imjgal">Insulated Monolithic Joints (IMG 1)</a></li>
	                <li><a href="images/products/Ins_Mont Joints_002.jpg" class="nyroModal" title="Insulated_Monolithic_Joints_2" rel="imjgal">Insulated Monolithic Joints (IMG 2)</a></li>
	                <li><a href="images/products/Ins_Mont Joints_003.jpg" class="nyroModal" title="Insulated_Monolithic_Joints_3" rel="imjgal">Insulated Monolithic Joints (IMG 3)</a></li>
	                <li><a href="images/products/Ins_Mont Joints_004.jpg" class="nyroModal" title="Insulated_Monolithic_Joints_4" rel="imjgal">Insulated Monolithic Joints (IMG 4)</a></li>
	                <!-- <li><a href="media/docs/Insulated_Monolithic_Joints.pdf">Insulated Monolithic Joints (PDF)</a></li> -->
	                <li><a href="#mono-video" class="nyroModal">Insulated Monolithic Joints (Video)</a></li>
            	</ul>
            	<div id="mono-video" style="display: none; width: 640px;">
				  <video width="640" height="480" controls>
				  	<source src="media/videos/monolithic-joint.mp4" type="video/mp4">
					Your browser does not support the video tag.
				  </video>
				</div>     
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
    </div> <!-- /rows -->

    <!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->
</body>
</html>
