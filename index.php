<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: WBI Desarrollos C.A. [www.wbinnova.com]; e-mail: info@wbinnova.com" />
    <meta name="copyright" lang="en" content="Webdesign: WBI Desarrollos C.A. [www.wbinnova.com]; e-mail: info@wbinnova.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<script type='text/javascript' src="js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
	<script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    	$.localScroll();
    });
    </script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - Supply Solutions for the Oil and Gas Industry </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
	<div id="promotions">
		<?php include 'promotions.php';?>
	</div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/promo1.jpg" alt="" /></li>
                <li><img src="images/promo2.jpg" alt="" /></li>
                <li><img src="images/p_home.jpg" alt="" /></li>
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->

    <!-- Three columns -->
    <div class="cols3">    
        <div class="cols3-content box">
            <!-- Column -->
            <div class="col">            
            	<img src="images/col-01.jpg"/>
                <h2><span>Teknacorp provides complete solutions &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="cs" onclick="javascript:slideText(this);return false;">more &#9002;</a></span></h2>
                <div id="cs-hidden" class="noscreen more"><p>We supply materials and services required by the Oil, gas and energy Industries representing the most prestigious brands in the market, providing the best quality and quickest delivery</p></div>          
            </div> <!-- /col -->
            
            <!-- Column -->
            <div class="col">
            	<img src="images/col-02.jpg"/>
                <h2><span>Supply Solutions for the Oil &amp; Gas Industry &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="ss" onclick="javascript:slideText(this);return false;">more &#9002;</a></span></h2>
                <div id="ss-hidden" class="noscreen more"><p>TEKNACORP provides a wide scope of procurement solutions including Piping, Instrumentation and General Maintenance Spare Parts. We combine our engineering and sourcing capability of specialized components in the US and International markets with traffic and logistic resources to provide an integral supply service. <br /><br /> 
                Our customer base is composed of Engineering and Construction Companies as well as Oil and Gas operators. Our main market of influence is the Latin America and the Caribbean region.</p></div>               
            </div> <!-- /col -->
            
            <!-- Column -->
            <div class="col last">
            	<img src="images/col-03.jpg"/>
                <h2><span>Products &amp; Services &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="ps" onclick="javascript:slideText(this);return false;">more &#9002;</a></span></h2>
                <div id="ps-hidden" class="noscreen more">
                	<p>Services and Engineering Solutions</p>
                	<ul>
                		<li>Express Solutions to your Supply Needs. We can place any component in your Jobsite in 48 Hrs if necessary.</li>
                		<li>Project Solutions for your Long Lead Bulk or Short Lead Requirements.</li>
                		<li>Logistic and Traffic Capability combining capacity with our Affiliate freight forwarders in USA and the Americas.</li>
                		<li>We have offices and regional agents located strategically in our areas of operation in USA, Venezuela, Mexico, Colombia and Trinidad.</li>
                	</ul>
                </div>
            </div> <!-- /col -->
        </div> <!-- /cols3-content -->
    </div> <!-- /cols3 -->
    
     <div class="rows">
    	<div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
	</div>
	
	<!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->

<script type="text/javascript">
var csMore = true;
var ssMore = true;
var psMore = true;
function slideText(element) {
	if(element.getAttribute("id") == "cs"){
		$('#cs-hidden').slideToggle();
		csMore ? $('#cs').html("less &#9001;") : $('#cs').html("more &#9002;");
		csMore = !csMore;
	}else if(element.getAttribute("id") == "ss"){
		$('#ss-hidden').slideToggle();
		ssMore ? $('#ss').html("less &#9001;") : $('#ss').html("more &#9002;");
		ssMore = !ssMore;
	}else if(element.getAttribute("id") == "ps"){
		$('#ps-hidden').slideToggle();
		psMore ? $('#ps').html("less &#9001;") : $('#ps').html("more &#9002;");
		psMore = !psMore;
	}
}
</script>
</body>
</html>
