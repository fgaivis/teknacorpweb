<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="copyright" lang="en" content="Webdesign: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<script type='text/javascript' src="js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
    <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    	$.localScroll();
    });
    </script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - Contact Us </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
	<div id="promotions">
		<?php include 'promotions.php';?>
	</div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/p_contacts.jpg" alt="" /></li>
                <!-- <li><img src="images/promo1.jpg" alt="" /></li>  -->
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->
    
    <h1 id="title">Contact Us</h1>
    
    <div style="margin: 0 auto; width:860px;">
	    <!-- <p>Click on the buttons on the bottom left to start the animation.</p> -->	    
	    <p>Our Offices and Operations</p>
	    <p> 
	    	<!-- <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="859" height="425" title="mapoffices">
	           <param name="movie" value="media/flash/map-offices.swf" />
	           <param name="quality" value="high" />
	           <embed src="media/flash/map-offices.swf" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="859" height="425"></embed>
	        </object> -->
	        <img src="media/videos/mapa-animado.gif" alt="AnimatedMap" width="859" height="425" />
	    </p>	    
	    <p>&nbsp;</p>
    </div>
    
    <!-- Content Rows -->
    <div class="rows separation">    
        <div class="rows-content box separator" id="pe">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/houston.jpg"/>
	            <h2><span>Offices - Teknacorp Group</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text content">
	            <table>
	                <tr>
	                    <th width="180px">City - Country</th>
	                    <th width="180px">Office</th>
	                    <th width="400px">Contact Information</th>
	                </tr>
	                <tr class="odd">
	                    <td width="180px">Houston, TX - USA</td>
	                    <td width="180px">TEKNACORP USA Inc.</td>
	                    <td width="400px">14300 Northwest  Fwy, Suite B14, Houston, TX 77040	Phone:  (713) 429 4149 / (713) 934 9059, Fax: (832) 202 2769. </td>
	                </tr>
	                <tr>
	                    <td width="180px">Caracas - Venezuela</td>
	                    <td width="180px">TEKNACORP 21, C.A.</td>
	                    <td width="400px">Av. Francisco Miranda, Edif. Saule. Of. 23, 25; Chacao, Caracas 1060, Venezuela.	Phone: +58 0212 265.81.11 - +58 212 266.06.14 - Fax: 266.03.26</td>
	                </tr>
	                <tr class="odd">
	                    <td width="180px">Ciudad del Carmen - Mexico</td>
	                    <td width="180px">TEKNACORP Suministros Especializados de M&eacute;xico S.A. de C.V.</td>
	                    <td width="400px">Av. Periférica Norte No. 10, Fraccionamiento Malibrán. Ciudad del Carmen. Campeche, México. Phone: +52 938 286 1667.</td>
	                </tr>
	                <!-- <tr>
	                    <td width="180px">Villahermosa - Mexico</td>
	                    <td width="180px">TEKNACORP</td>
	                    <td width="400px">Mario Trujillo, Nro. 288. 86220 Lomitas, Nacajuja, Tabasco, Mexico. Phone: +52 914 336 7415 / +521 9931 933 222 .</td>
	                </tr> -->
	                <tr class="odd">
	                    <td width="180px">Bogot&aacute; - Colombia</td>
	                    <td width="180px">TEKNACORP Colombia SAS</td>
	                    <td width="400px">Calle 125, No. 19-89, Oficina 504; Santa Barbar&aacute;, Bogot&aacute;, Colombia. Phone: +57 1 658 3781 - Mobile: +57 320 233 1309</td><!-- <a href="mailto:salescolombia@teknacorp.com">salescolombia@teknacorp.com</a> -->
	                </tr>
	            </table>
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="sol">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/offices.jpg"/>
	            <h2><span>Departments - Contact info</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text content">            
	            <table>
	                <tr>
	                    <th>Department</th>
	                    <th>Email Contact</th>
	                </tr>
	                <tr class="odd">
	                    <td><b>Direction and Management</b></td>
	                    <td><a href="mailto:director@teknacorp.com">director@teknacorp.com</a></td>
	                </tr>
	                <tr>
	                    <td><b>Regional Sales</b></td>
	                    <td>
	                    	<table class="no-border">
	                        	<tr>
	                                <td>USA:</td>
	                                <td><a href="mailto:salesusa@teknacorp.com">salesusa@teknacorp.com</a></td>
	                            </tr>
	                            <tr>
	                                <td>Venezuela:</td>
	                                <td><a href="mailto:salesven@teknacorp.com">salesven@teknacorp.com</a></td>
	                            </tr>
	                            <tr>
	                                <td>Mexico:</td>
	                                <td><a href="mailto:salesmexico@teknacorp.com">salesmexico@teknacorp.com</a></td>
	                            </tr>
	                            <tr>
	                                <td>Colombia:</td>
	                                <td><a href="mailto:salescolombia@teknacorp.com">salescolombia@teknacorp.com</a></td>
	                            </tr>
	                            <tr>
	                                <td>Trinidad:</td>
	                                <td><a href="mailto:salestrinidad@teknacorp.com">salestrinidad@teknacorp.com</a></td>
	                            </tr>
	                            <tr>
	                                <td>Peru:</td>
	                                <td><a href="mailto:salesperu@teknacorp.com">salesperu@teknacorp.com</a></td>
	                            </tr>
	                            <tr>
	                                <td>Chile:</td>
	                                <td><a href="mailto:saleschile@teknacorp.com">saleschile@teknacorp.com</a></td>
	                            </tr>
	                            <tr>
	                                <td>Argentina:</td>
	                                <td><a href="mailto:salesargentina@teknacorp.com">salesargentina@teknacorp.com</a></td>
	                            </tr>
	                        </table>
	                    </td>
	                </tr>
	                <tr class="odd">
	                    <td><b>Engineering</b></td>
	                    <td><a href="mailto:engineering@teknacorp.com">engineering@teknacorp.com</a></td>
	                </tr>
	                <tr>
	                    <td><b>Quality Assurance</b></td>
	                    <td><a href="mailto:qualityassurance@teknacorp.com">qualityassurance@teknacorp.com</a></td>
	                </tr>
	                <tr class="odd">
	                    <td><b>Accounting</b></td>
	                    <td><a href="mailto:accounting@teknacorp.com">accounting@teknacorp.com</a></td>
	                </tr>
	                <tr>
	                    <td><b>General Information</b></td>
	                    <td><a href="mailto:info@teknacorp.com">info@teknacorp.com</a></td>
	                </tr>
	                <tr class="odd">
	                    <td><b>Vendors</b></td>
	                    <td><a href="mailto:vendors@teknacorp.com">vendors@teknacorp.com</a></td>
	                </tr>
	                <tr>
	                    <td><b>Webmaster</b></td>
	                    <td><a href="mailto:webmaster@teknacorp.com">webmaster@teknacorp.com</a></td>
	                </tr>
	            </table>
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
    </div> <!-- /rows -->

    <!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->
</body>
</html>
