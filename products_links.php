<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="copyright" lang="en" content="Webdesign: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<script type='text/javascript' src="js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
    <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    	$.localScroll();
    });
    </script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - Products </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/promo1.jpg" alt="" /></li>
                <!-- <li><img src="images/promo2.jpg" alt="" /></li>  -->
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->
    
    <h1 id="title">Products</h1>
    
    <!-- Content Rows -->
    <div class="rows separation">    
        <div class="rows-content box separator" id="pipes">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/pipes.jpg"/>
	            <h2><span class="products">Pipe</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li><a href="media/docs/PIPE_GRADES.pdf">Line pipe as per API5L</a></li>
	                <li><a href="media/docs/PIPE_GRADES.pdf">Process Pipe as per ASTM</a></li>
	                <li><a href="media/docs/PIPE_GRADES.pdf">Carbon, Chrome and Stainless</a></li>
	                <li><a href="media/docs/PIPE_GRADES.pdf">Structural Pipe API 2H</a></li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="fittings">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/fittings.jpg"/>
	            <h2><span class="products">Fittings</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li><a onclick="javascript:showButtWeld();" style="cursor:pointer; color:#009;">Butt Weld</a></li>
	                <li><a onclick="javascript:showForgedSteel();" style="cursor:pointer; color:#009;">Forged Steel</a></li>
	                <li><a onclick="javascript:showSpecialT();" style="cursor:pointer; color:#009;">Special Tee's</a></li>
	                <li>Split tee Plugged Tee</li>
	                <li>Radial Flow Tees</li>
	                <li>Barred Tee</li>
	                <li>Special Extruded Headers</li>
	            </ul>      
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="flanges">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/flanges.jpg"/>
	            <h2><span class="products">Flanges</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li><a onclick="javascript:showFlanges();" style="cursor:pointer; color:#009;">Flanges</a></li>
	            </ul>      
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="filters">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/filters.jpg"/>
	            <h2><span class="products">Filters</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li><a onclick="javascript:showFilters();" style="cursor:pointer; color:#009;">Filters</a></li>
	                <li>Quick Opening Closure</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="valves">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/valves.jpg"/>
	            <h2><span class="products">Valves</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li><a onclick="javascript:showGate();" style="cursor:pointer; color:#009;">Gate, Globe, Check</a></li>
	                <li><a onclick="javascript:showBall();" style="cursor:pointer; color:#009;">Ball Valves</a></li>
	                <li><a onclick="javascript:showPowerPlants();" style="cursor:pointer; color:#009;">Power Plants Valves</a></li>
	                <li><a onclick="javascript:showForgedValves();" style="cursor:pointer; color:#009;">Forged Gate, Globe, Piston, Swing & Ball Check Valves</a></li>
	                <li><a onclick="javascript:showCastIron();" style="cursor:pointer; color:#009;">Cast Iron Valves</a></li>
	                <li><a onclick="javascript:showButterfly();" style="cursor:pointer; color:#009;">Butterfly Valves</a></li>
	                <li><a onclick="javascript:showBronze();" style="cursor:pointer; color:#009;">Bronze Valves</a></li>
	                <li><a onclick="javascript:showWafer();" style="cursor:pointer; color:#009;">Wafer Check Valves</a></li>
	                <li><a onclick="javascript:showPlug();" style="cursor:pointer; color:#009;">Plug Valves</a></li>
	                <li><a onclick="javascript:showTubeValves();" style="cursor:pointer; color:#009;">Tube Fittings &amp; Valves</a></li>
	                <li><a onclick="javascript:showSafetyValves();" style="cursor:pointer; color:#009;">Safety Relief Valves</a></li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="coatings">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/coatings_fbe.jpg"/>
	            <h2><span class="products">Coatings</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Epoxyco</li>
	                <li>FBE</li>
	                <li>Polyurethane</li>
	                <li>Internal &amp; External</li>
	                <li>Special coatings</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="bends">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/bends.jpg"/>
	            <h2><span class="products">Bends</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul class="links-list">
	                <li>Diameters from 1/2" to 66"</li>
	                <li>Short and Long radius, 3D &amp; 5D</li>
	                <li>Carbon Steels, Stainless, Diverse Alloys</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
    </div> <!-- /rows -->

    <!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->
</body>
</html>
