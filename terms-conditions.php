<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="copyright" lang="en" content="Webdesign: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<script type='text/javascript' src="js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
    <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    	$.localScroll();
    });
    </script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - Terms and Conditions </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
	<div id="promotions">
		<?php include 'promotions.php';?>
	</div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/p_terms.jpg" alt="" /></li>
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->
    
    <h1 id="title">Terms &amp; Conditions</h1>
    <span style="margin-left: 80px; font-size: 12px; color: #333;">Download the General Terms and Conditions / Technical Notes Document: <a href="media/docs/GENERAL_TERMS_AND_CONDITIONS_OF_TEKNACORP_ENGLISH.pdf" style="color: #009">English</a> / <a href="media/docs/TERMINOS_Y_CONDICIONES_TEKNACORP_ESPANOL.pdf" style="color: #009">Spanish</a></span>
    <br/><br/>
    
    <!-- Content Rows -->
    <div class="rows separation">    
        <div class="rows-content box separator" id="tac">
	    	<div class="row">            
	            <img src="images/mission.jpg"/>
	            <h2><span class="distribution">Terms and Conditions</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text" style="font-size: 11px;">            
	            <p><b>GENERAL TERMS AND CONDITIONS</b></p>
	            <p>1. PRICES</p>
	            <p>All prices negotiated are in US DOLLARS, (Taxes are not included) Prices quoted are based on total quantity contained in this quotation. If quantities vary, TEKNACORP reserves the right to re-quote accordingly. The minimum order value is $1,000.00 (One thousand dollars), unless otherwise agreed to by TEKNACORP. If for some reason any items or delivery date(s) are changed or additions to the order required, TEKNACORP reserves the right to adjust prices accordingly. All sales are subject to approval of TEKNACORP’s credit department. If buyer fails to meet the agreed upon and established commercial terms of the contract, TEKNACORP may withhold all subsequent deliveries until such time that the original commercial terms of the contract have been met by the buyer (or subsequent commercial terms have been agreed upon by TEKNACORP with the buyer).</p>
	            <p>2. DELIVERY</p>
	            <p>TEKNACORP’s acceptance of the Purchase Order shall occur at the time Buyer receives TEKNACORP’s acknowledgment of acceptance and Payment Terms have been agreed.  Any provision in any order providing that time of delivery is of the essence is expressly rejected.  Material is subject to prior sales. Ship dates are based on current shop loads and inventories and are subject to revision at time of order placement.  Shipment dates offered are forecasted delivery lead times and are estimated from the date of purchase order acknowledgement, clarification is received on all technical information, and resolution of customer’s written approval of drawings is received (when required). Title and ownership of the Products shall pass to Buyer upon delivery of the Products to Buyer or Buyer’s authorized representative, and full payment made by Buyer to TEKNACORPfor the Products.   </p>
	            <p>3. SHIPPING TERMS</p>
	            <p>All items quoted are Ex-Works / FOB our Dock in Houston, Texas, unless specified in our quotation. TEKNACORP fulfills the obligation to deliver when the goods are made available at TEKNACORP’s premises. TEKNACORP agrees (on TEKNACORP’s premises) to load the items on the buyers scheduled mode of transportation unless shipping and INCOTERM delivery terms have been agreed by TEKNACORP and the buyer prior. The material quoted shall be packed in accordance with TEKNACORP’s standard packing procedure. Any additional packaging requesting by the buyer is the responsibility of the buyer, unless otherwise agreed to in writing by TEKNACORP. </p>
	            <p>4. PAYMENT TERMS</p>
	            <p>As determined in TEKNACORP’s quotation. All accounts are payable in U.S. currency, unless otherwise agreed in writing by TEKNACORP, free and net of bank exchange, bank charges, collection or other charges. If Buyer fails to fulfill the terms of payment or if TEKNACORP shall have any doubt at any time as to Buyer’s financial responsibility, capacity or insolvency, TEKNACORP may suspend, at its sole discretion, production and/or decline to make shipment or delivery of the Products except upon receipt of cash or security satisfactory to TEKNACORP. </p>
	            <p>5. VALIDITY</p>
	            <p>
	            	(*) This quotation is valid for the indicated number of days shown of the quotation Validity on special metals, including Stainless Steel, will depend on the stability of the price markers as per London’s Metal Exchange.<br />
	            	(*) All products offered from stock are subject to prior sale.
	            </p>
	            <p>6. CANCELLATION POLICY</p>
	            <p>No contract may be canceled by the buyer except upon written notice to TEKNACORP and upon payment to TEKNACORP of all costs incurred by the contract arising out of, or in connection with, the contract. Export of goods covered hereby is subject to United States Customs Control. Standard stocking items will be subject to a twenty-five percent (25%) restocking and/or cancellation charge. Non-standard stocking items will be subject to a one hundred percent (100%) restocking and/or cancellation charge.<br />
					Cancellation Charges:<br />
					The following indicates the rates of cancellation charge of contract value for project manufactured items and/or special engineered items at various stages of production:<br />
					1. After Order Acknowledgement and prior to engineering Documentaion (Drawings, Designs or Calculations). Cancellation Charge: 10%<br />
					2. After start of engineering but prior to release to production. Cancellation Charge: 30%<br />
					3. After release to production but prior to completion of fabrication. Cancellation Charge: 80%<br />
					4. After completion of fabrication. Cancellation Charge: 100%<br />
	            </p>
	            <p>7. FORCE MAJEURE</p>
	            <p>If in the case of an act of God, war, riot, fire, explosion, flood, or any other circumstances of whatsoever nature which are beyond the control of TEKNACORP and which in any way affect the ability of TEKNACORP to fulfill its delivery obligations, the delivery is hindered, impeded, or delayed TEKNACORP shall be exonerated from all responsibilities and reserves the right to postpone the delivery beyond the original schedule. If, as a result of Force Majeure, it becomes impossible or impractical for either party to carry out its obligations hereunder (other than any obligation to pay money when due in accordance with the terms of the Standard Terms and Conditions) in whole or in part, then such obligations shall be suspended to the extent necessary by such Force Majeure during its continuance.</p>
	            <p>8. WARRANTY</p>
	            <p>
	            	Teknacorp warrants that the Products purchased by buyer shall: <br />
					(a) Conform to the specifications prescribed in the relevant Purchase Order; <br />
					(b) Be manufactured, and/or assembled in accordance with good design, engineering, fabrication, and workmanship practices<br />
					(c) Be in good working conditions<br />
					(d) Comply, in all material respects, with all laws, regulations and orders applicable to its operations and performance hereunder.<br />
					Products sold by TEKNACORP are guaranteed against defects in workmanship for a period of twelve (12) months after being placed in service, but not exceeding 
eighteen (18) months after shipment, when products are properly installed per manufacturer’s specifications and used within the service and pressure range for which they were manufactured. Full risk of loss shall pass to the buyer upon delivery Ex-works, or destination port in case of CIF. This warranty is limited to the replacement or repair of any material found to be defective either in material or workmanship. The unauthorized use of third party components and workmanship in TEKNACORP’s products voids this warranty.<br />
					The foregoing warranties are the exclusive warranties made by TEKNACORP with respect to defective products, and buyer hereby waives and TEKNACORP disclaims all other warranties, whether oral, written, expressed, implied or statutory including but not limited to warranties of merchantability or fitness for a particular or intended purpose. The remedies available to buyer under this warranty provision are the exclusive remedies of buyer with respect to defects, and all other remedies available to buyer are hereby waived and excluded.	            	
	            </p>
	            <p>9. LIMITATION OF LIABILITY</p>
	            <p>The liability of TEKNACORP under this agreement or with respect to any products supplied or services performed pursuant to this agreement, whether in contract, in tort, in strict liability or otherwise, shall not exceed the purchase price paid by the buyer with respect thereto. In no event will TEKNACORP be liable in contract, in tort, in strict liability or otherwise for any special, indirect, incidental, or consequential damages. This is including, but not limited to loss of anticipated profits or revenues, loss of use, non-operation or increased expense of operation of equipment, cost of capital, or claims from customer or buyer for failure or delay in achieving anticipated profits. TEKNACORP shall not be responsible for technical advice given by, or acted upon from, Buyer or any third party in connection with the design, installation or use of any product. </p>
	            <p>10. CONFIDENTIALITY AND INTELLECTUAL PROPERTY</p>
	            <p>Confidential Information shall mean the specifications, illustrations, drawings, certificates, designs, documentation, software, know-how, and all of that which is identified by TEKNACORP to be of proprietary and/or confidential nature which is to be delivered or disclosed to Buyer related to any Purchase Order., but shall not include any information TEKNACORP acquires from third parties without restrictions prohibiting its disclosure, use or license.   Buyer agrees to hold in confidence and refrain from disclosing to any person, except Buyer's employees having a need to know, any Confidential Information related to any Purchase Order without first obtaining TEKNACORP's written consent. Buyer also agrees not to use any Confidential Information for purposes not related to the Purchase Order to which it is related.  Such restrictions will not apply to any Confidential Information that:</p>
	            <p>
	            	(a) Buyer can show by written records are published or come into the public domain without restriction through no act or fault of Buyer, or <br />
	            	(b)	Buyer can demonstrate was known to Buyer prior to disclosure by TEKNACORP, or thereafter becomes known by Buyer from any source (other than TEKNACORP) having a right to disclose without restriction. 
	            </p>
	            <p>11. CONTROLLING LAW</p>
	            <p>All questions  concerning the validity and operation of the Agreement and the performance of the obligations imposed upon the parties hereunder shall come within the exclusive jurisdiction of Courts located in the State of Texas and shall be governed by the laws of the State of Texas without reference to its conflict of laws rules.</p>
	            <p>The Purchase	Order	together with the Standard Terms and Conditions, including expressly incorporated attachments,	constitute the entire agreement between BUYER and TEKNACORP regarding the purchase and supply of the Products and services, and supersedes all prior bids, awards, discussions, negotiations and agreements regarding the subject matter.  Technacorp expressly takes exception to any other terms and conditions, including any pre-printed terms may appear on buyer's purchase_order.</p>
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="technical">
	    	<div class="row">            
	            <img src="images/people.jpg"/>
	            <h2><span class="distribution">Technical Notes</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text" style="font-size: 11px;">   
	        	<p><b>12. TECHNICAL NOTES / EXCEPTIONS</b></p>         
	            <p>12.1. It is the Buyer’s responsibility to review any exceptions taken with the end user. If, for any reason, there is a concern or need for clarification of any exception mentioned herein, please notify TEKNACORP. If no notification is made, it will be understood that the Buyer and /or end user have accepted the exceptions.</p>
	            <p>12.2. No Coatings or Paint (Internal and External) is being offered unless otherwise noted in the offer </p>
	            <p>12.3. Not included in the offers:</p>
	            <p>- Installations and Start UP Services or Equipment (Assumed supplied by others)<br />
					- Site acceptance testing (Assumed supplied by others) <br />
					- Third Party Inspection <br />
					- Witness, Inspection or Audits 
	            </p> 
	            <p>Note: NO ADDITIONAL COMPONENTS, TESTING OR DOCUMENTATION IS BEING OFFERED UNLESS OTHERWISE STATED IN THIS OFFER.</p>
	            <p>12.4.Documentation</p>       
	            <p>Standard Documentation included (Where applicable): Material Test Reports (MTR), Hydrostatic Test report. No other documentation is being offered as Standard. If additional testing and documentation is required, additional charges will apply.</p>
	            <p>Project Specifications, Data Sheets or Drawings observance. TEKNACORP is willing to revise quotations if required to meet specific customer’s specifications if not declared at time of quote.</p>
	            <p>12.5. Inspection</p>
	            <p>The Buyer or his authorized representative may, with four (4) weeks prior notice given to TEKNACORP, visually inspect products manufactured by TEKNACORP. Such TEKNACORP-approved inspections will be carried out in accordance with TEKNACORP’s standard or  approved customer inspection procedures. If any inspection or documentation requested by the buyer is over and beyond the scope and criteria initially agreed to by TEKNACORP, any costs incurred by conducting such inspection or preparation of special documents shall be paid by the buyer prior to release of the items for shipment.</p>
	            <p>12.6. Witness Hydro-Testing (where applicable)</p>
	            <p>Witness Hydro testing is available at a cost. A scope of buyers inspection request is to be provided to TEKNACORP at order placement. Late notice of such requested inspection is subject to additional costs. The cost associated with such witness hydro request is to be agreed on prior to any such testing taking place. Payment of this type of testing to be negotiated. Additionally, any costs associated with a third party inspector will not be at TEKNACORP’s expense.</p>
	            <p><b>ADDITIONAL NOTES</b></p>
	            <p>The material shown in this quotation is based upon TEKNACORP’s interpretation of your Technical Specification or other form of request for quotation. Buyer is responsible for and should ensure that the material quoted meet it’s engineering or design requirements or those of buyer’s customer. TEKNACORP reserves the right to issue a revised quotation upon receipt of additional customer requirements and appropriate charges may apply.</p>
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
    </div> <!-- /rows -->

    <!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->
</body>
</html>
