
 	<!-- Navigation -->
        <div id="nav">        
          <div class="topmenu">
             <div>
                <ul class="topnav">
                    <li><a href="index.php">Home</a></li>  
                    <li><a href="about.php">About Us</a></li>  
                    <li><a href="experience.php">Experience</a></li>
                    <li><a href="products.php">Products</a>  
                        <ul class="subnav">  
                        <li><a href="products.php#pipes">Pipes</a></li>
                        <li><a href="products.php#bends">Bends</a></li>
                        <li><a href="products.php#fittings">Fittings</a></li>
                        <li><a href="products.php#flanges">Flanges</a></li>
                        <li><a href="products.php#swivel-flanges">Swivel Flanges</a></li>
                        <li><a href="products.php#cladding">Clad Products</a></li>  
                        <li><a href="products.php#strainers">Strainers</a></li>
                        <li><a href="products.php#closures">Closures</a></li>
                        <li><a href="products.php#valves">Valves</a></li>
                        <li><a href="products.php#coatings">Coatings</a></li>
                        <li><a href="products.php#pig-launchers">Pig Launchers / Receivers</a></li>
                        <li><a href="products.php#im-joints">Insulated Monolithic Joints</a></li>
                      </ul>  
                    </li>
                    <li><a href="services.php">Services</a>  
                        <ul class="subnav">  
                        <li><a href="services.php#pe">Project Engineering</a></li>  
                        <li><a href="services.php#sol">Solutions</a></li>  
                        <li><a href="services.php#log">Logistics</a></li>
                      </ul>  
                    </li>  
                    <!-- <li><a href="distributions.php">Distributions</a></li> -->
                    <li><a href="terms-conditions.php">Terms &amp; Conditions</a></li>
                    <li class="last"><a href="contact.php">Contact</a></li> 
                </ul>
             </div>
          </div><!-- /topmenu -->
        </div><!-- /nav -->
        