
	<!-- Footer -->
    <div id="footer" class="box">

        <p class="f-left"><a href="index.php"><img src="images/logo_grey.png" alt="Teknacorp" /></a></p>     
        <p id="footnav" class="f-right">
            <a href="about.php">About Us</a>
            <a href="experience.php">Experience</a>
            <a href="products.php">Products</a>
            <a href="services.php">Services</a>
            <!-- <a href="distributions.php">Distributions</a> -->
            <a href="contact.php">Contact</a>
        </p>
        
        <div id="addresses" class="f-left">
        	<table>
        		<thead>
        			<tr>
        				<td>USA</td>
        				<!-- <td>Venezuela</td> -->
        			</tr>
        		</thead>
        		<tbody>
        			<tr>
        				<td>14300 Northwest Fwy,</td>
        				<!-- <td>Av. Francisco Miranda,</td> -->
        			</tr>
        			<tr>
        				<td>Suite B14,</td>
        				<!-- <td>Edif. Saule. Of. 24,</td> -->
        			</tr>
        			<tr>
        				<td>Houston, TX 77040</td>
        				<!-- <td>Chacao, Caracas 1060</td> -->
        			</tr>
        			<tr>
        				<td>Phone: (713) 429-4149</td>
        				<!-- <td>Telf: +58 (0212) 265-8111</td> -->
        			</tr>
        			<tr>
        				<td>(713) 934-9059</td>
        				<!-- <td>+58 (212) 266-0614</td> -->
        			</tr>
        			<tr>
        				<td>Fax: (832) 202-2769</td>
        				<!-- <td>Fax: +58 (212) 266-0326</td> -->
        			</tr>
        		</tbody>
        	</table>
        </div>
        <div id="footlinks" class="f-right">
        	<table>
        		<tr>
        			<td class="first"><a href="about.php#mission">Mission</a></td>
        			<td class="mid"><a href="experience.php#process-pipe">Process Pipe</a></td>
        			<td class="mid"><a href="products.php#pipes">Pipes</a></td>
        			<td><a href="services.php#pe">Project Engineering</a></td>
        		</tr>
        		<tr>
        			<td class="first"><a href="about.php#vision">Vision</a></td>
        			<td class="mid"><a href="experience.php#radial-flow">Radial Flow Tees</a></td>
        			<td class="mid"><a href="products.php#bends">Bends</a></td>
        			<td><a href="services.php#sol">Solutions</a></td>
        		</tr>
        		<tr>
        			<td class="first"><a href="about.php#quality-policy">Quality Policy</a></td>
        			<td class="mid"><a href="experience.php#welded-body">Welded Body</a></td>
        			<td class="mid"><a href="products.php#fittings">Fittings &amp; Flanges</a></td>
        			<td><a href="services.php#log">Logistics</a></td>
        		</tr>
        		<tr>
        			<td class="first"><a href="about.php#people">Our People</a></td>
        			<td class="mid"><a href="experience.php#flanges">Flanges</a></td>
        			<td class="mid"><a href="products.php#cladding">Cladding</a></td>
        			<td>&nbsp;</td>
        		</tr>
        		<tr>
        			<td class="first"><a href="about.php#values">Values</a></td>
        			<td class="mid"><a href="experience.php#clients">Our Clients</a></td>
        			<td class="mid"><a href="products.php#strainers">Strainers</a></td>
        			<td>&nbsp;</td>
        		</tr>
        		<tr>
        			<td class="first">&nbsp;</td>
        			<td class="mid">&nbsp;</td>
        			<td class="mid"><a href="products.php#valves">Valves</a></td>
        			<td>&nbsp;</td>
        		</tr>
        		<tr>
        			<td class="first">&nbsp;</td>
        			<td class="mid">&nbsp;</td>
        			<td class="mid"><a href="products.php#coatings">Coatings</a></td>
        			<td>&nbsp;</td>
        		</tr>
        	</table>
        </div>

    </div> <!-- /footer -->
    <div id="bottom"></div>
    <?php include_once("analyticstracking.php") ?>
