<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="copyright" lang="en" content="Webdesign: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<script type='text/javascript' src="js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
    <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    	$.localScroll();
    });
    </script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - Experience </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
	<div id="promotions">
		<?php include 'promotions.php';?>
	</div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/promo2.jpg" alt="" /></li>
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->
    
    <h1 id="title">Experience</h1>
    <h2 id="subtitle">Some of our success stories</h2>
    
    <!-- Content Rows -->
    <div class="rows separation">    
        <div class="rows-content box separator" id="process-pipe">
	    	<div class="row">            
	            <img src="images/process-pipe.jpg"/>
	            <h2><span class="distribution">Process Pipe</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text" style="text-align: justify;">            
	            <ul>
	            	<li>The pipe (electric fusion welded Pipe, alloy steel SCH 30 ASTM A -691 GR, 2 ¼ CR CL 22 beveled ends for welding in B16, 25 ASME V, ASME B36,10 dimensions, additional requirements Complementary S52 And S63)   was  supervised and managed  by Teknacorp from the initial  process of the base plate, the rolling of the pipe, quality testing  and heat treatment to final delivery to the final destination in the Argentinian refining System.</li>
	            	<li>The Customer requirement included compliance with ASTM and UOP technical standard Due to the demands of the mechanical properties required by the Project, Teknacorp’s Engineers designed a   heat treatment procedure and involving step cooling to achieve the required impact values.</li>
	            </ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="radial-flow">
	    	<div class="row">            
	            <!-- <img src="images/radial-flow.jpg"/> -->
	            <img src="images/radial-flow-tee.jpg"/>
	            <h2><span class="distribution">Radial Flow Tees</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>Teknacorp has the capacity of manufacturing special fittings made to order.  One success case is radial flow tees which are used in the Mexican offshore industry.  In this Case Teknacorp Engineers designed a 36” TEE, MSS SP 75 WPY75, 100 RX WELDED ACCORING TO PEMEX STANDARDS.  THE DESIGN OF The Radial Flow Tee design included calculating the non-obstructing surface area of the jacket to be larger or equal to the cross sectional area or the outlet to avoid pressure and flow drop.</p>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="welded-body">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/welded-body.jpg"/>
	            <h2><span class="distribution">Welded Body Ball Valves</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul>
	                <li>Manufacture of welded body valves for service in the marine environment</li>
	                <li>The valves have body materials, internal and special trim, in compliance with Standard PEMEX NRF-211 required by Off Shore Mexican market.</li>
	                <li>Manufacturing in a record time</li>
	                <li>Anti Corrosive Finishing</li>
            	</ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="flanges">
	    	<div class="row">            
	            <!-- <img src="images/misalignment-flanges.jpg"/> -->
	            <img src="images/misa-swiv-flanges.jpg"/>
	            <h2><span class="distribution">Misalignment Flanges / Swivel Flanges</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p><b>Misalignment Flanges</b> (MAF) self aligning ball joint flange of metal-to-metal connector for joining together two misaligned pipelines. It is generally used in the offshore industry. Teknacorp has supplied MAF for PEMEX offshore in 16”, special bore, ASTM A 694 Gr F 52, MSS SP-44 according to PEMEX NRF-096-2010 and also <b>Swivel Flanges</b> (S.R.F) for RTJ configuration sour service. Project range from 8” to 16” pressure rating 600#.</p>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" style="margin-bottom: 0; padding-bottom: 0;" id="clients">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/clients.jpg"/>
	            <h2><span class="distribution">Our Clients</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text" style="width: 680px;">            
	            <ul>
	                <li>Cardon IV - <i>(Venezuela)</i></li>
	                <li>Compa&ntilde;&iacute;a Operadora del Gas del Amazonas [COGA] - <i>(Peru)</i></li>
	                <li>Constructora Norberto Odebrecht S.A. - <i>(Argentina)</i></li>
	                <li>Construtec  Villahermosa S.A de C.V - <i>(M&eacute;xico)</i></li>
	                <li>Contugas - <i>(Peru)</i></li>
	                <li>Cotemar S.A de C.V - <i>(M&eacute;xico)</i></li>
	                <li>Ecopetrol América - <i>(Colombia)</i></li>
	                <li>Equion Energ&iacute;a - <i>(Colombia)</i></li>
	                <li>Inelectra - <i>(Venezuela)</i></li>	                
	                <li>Ismocol - <i>(Colombia)</i></li>
	                <li>Norpower-Tecpetrol - <i>(M&eacute;xico)</i></li>
	                <li>Oceanografia S.A de C.V - <i>(M&eacute;xico)</i></li>	                
	                <li>Oleoducto Bicentenario de Colombia [OBC] - <i>(Colombia)</i></li>
	                <li>Oleoducto de los Llanos Orientales [ODL] - <i>(Colombia)</i></li>	                
	                <li>Pacific Rubiales Energy - <i>(Colombia)</i></li>          
	                <li>PDVSA - <i>(Venezuela)</i></li>
	                <li>Pentech</li>
	                <li>Petronaval S.A de C.V - <i>(M&eacute;xico)</i></li>
	                <li>PLUSPETROL - <i>(Peru)</i></li>
	                <li>SubSea7 - <i>(M&eacute;xico)</i></li>
	                <li>Swiber Offshore México - <i>(M&eacute;xico)</i></li>
	                <li>Techint - <i>(Argentina)</i></li>        
	                <li>Transportadora de Gas del Per&uacute; [TgP] - <i>(Peru)</i></li>
	                <li>Weir Minerals Latin America - <i>(Chile)</i></li>
	                <li>Ypergas - <i>(Venezuela)</i></li>
	                <li>Ypf - <i>(Argentina)</i></li>
	            </ul>        
	        </div> <!-- /row-text -->
	        <div class="clients-img" style="position: relative; top: -590px; left: -50px; float: right; width: 220px; height: 100px;"><img src="images/clients.png" /></div>
	    </div> <!-- /rows-content -->
	    <!-- <div class="rows-content box" id="projects">
	    	<!-- <div class="row" style="margin-right: 46px;">            
	            <img src="images/projects.jpg"/>
	            <h2><span class="distribution">Our Projects</span></h2>            
	        </div> <!-- /row -->
	        <!-- <div class="row-text">            
	            <ul>
	                <li><b>Oleoducto de los Llanos Orientales</b> - ODL - <i>(Colombia)</i></li>
	                <li><b>Oleoducto Bicentenario de Colombia</b> - OBC - <i>(Colombia)</i></li>
	                <li><b>Cia. Tec. Int. SACI</b> - TECHINT - <i>(Argentina)</i> </li>
	                <li><b>Transportadora de Gas del Pe&uacute;</b> - TgP - <i>(Per&uacute;)</i> </li>
	                <li><b>Oceanografia</b> - OCEANOGRAFIA - <i>(M&eacute;xico)</i> </li>
	            </ul>     
	        </div> <!-- /row-text -->
	    <!-- </div> --> <!-- /rows-content -->
	    <div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
    </div> <!-- /rows -->

    <!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->
</body>
</html>
