<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="copyright" lang="en" content="Webdesign: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<script type='text/javascript' src="js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
    <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    	$.localScroll();
    });
    </script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - Distributions </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
	<div id="promotions">
		<?php include 'promotions.php';?>
	</div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/p_distributions.jpg" alt="" /></li>
                <!-- <li><img src="images/promo2.jpg" alt="" /></li>  -->
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->
    
    <h1 id="title">Distributions</h1>
    
    <!-- Content Rows -->
    <div class="rows separation">    
        <div class="rows-content box separator" id="canadoil">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/canadoil.png"/>
	            <h2 class="distrib"><span class="distribution">Canadoil Group</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">
	        	<p>The Canadoil Group specializes in the manufacturing of large diameter pipe and fittings. Fittings are made in both welded and seamless from 1/2" to 102" (including heavy wall, high yield WPHY-100) and pipe in thickness up to 2 1/2" in all types of alloys and steels (including low temperature and high yield API 5L-X80). We can supply large volumes of materials in very short lead times and quickly custom fabricate various piping assemblies to our customer's exact requirements.</p>     
	            <p>Download from our page additional info from this prestigious brand:</p>
	            <ul class="links-list">
	                <li><a href="media/docs/canadoil/GroupCatalog.pdf">Canadoil - Group Catalog</a></li>
	                <li><a href="media/docs/canadoil/ProductSpecifications.pdf">Canadoil - Product Specifications</a></li>
	                <li><a href="media/docs/canadoil/ProductionRange.pdf">Canadoil - Production Range</a></li>
	                <li><a href="media/docs/canadoil/CASIA1.mpg">Promotional Video for the Canadoil Group</a></li>
	                <li><a href="media/docs/canadoil/CPIPE1.wmv">Promotional Video for the Canadoil Products</a></li>
	                <li class="undown">Website: <a href="http://www.canadoilgroup.com/">www.canadoilgroup.com</a></li>
	            </ul>       
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <!-- <div class="rows-content box separator" id="cdi">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/cdi.png"/>
	            <h2><span class="distribution">CDI</span></h2>            
	        </div> <!-- /row -->
	        <!-- <div class="row-text">            
	            <p>CDI is an engineering and manufacturing firm that makes available a wide variety of pipeline pig location, tracking, passage detection and time-based benchmarking equipment for both portable and permanent installation purposes.</p>     
	            <p>Download from our page additional info from this prestigious brand:</p>
	            <ul class="links-list">
	                <li><a href="media/docs/cdi/BanditManual-Spanish.pdf">CD52 Bandit - Indicador de paso de Diablos No-Intrusivo</a></li>
	                <li><a href="media/docs/cdi/CD52-BanditPigDetector.pdf">CD52 Bandit - Non-Intrusive Pig Passage Indicator</a></li>
	                <li><a href="media/docs/cdi/CARTACDI.pdf">Representation Letter</a></li>
	            </ul>      
	        </div> --> <!-- /row-text -->
	    <!-- </div> --> <!-- /rows-content -->
	    <div class="rows-content box separator" id="scv">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/scv.png"/>
	            <h2 class="distrib"><span class="distribution">Southern California Valves</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>The product lines include commodity valves as well as specialty valves in all Sizes & Pressure Classes & Metallurgy; including Carbon Steel, Stainless Steel & Exotic Alloys. The valve types include Gate, Globe, Swing Check - Bolted Bonnet & Pressure Seal Bonnet, Ball - both floating and trunnion, Thru-Conduit Gate - slab and expanding, Swing Check - Full Port, Plugs, Wafer Checks and Butterflies.<br /> Southern California Valve holds the API6A & API6D Monogram, API Q1 Quality Management System, and ASME "R" stamp.</p>     
	            <p>Download from our page additional info from this prestigious brand:</p>
	            <ul class="links-list">
	                <li><a href="media/docs/scv/SCVCatalog2009.pdf">SCV - Product Catalog</a></li>
	                <!-- <li><a href="media/docs/scv/CARTASCV.pdf">Representantion Letter 1</a></li>
	                <li><a href="media/docs/scv/CARTASCV2.pdf">Representantion Letter 2</a></li> -->
	                <li class="undown">Website: <a href="http://www.scvvalve.com/">www.scvvalve.com</a></li>
	            </ul>       
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="tianjin">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/tianjin.png"/>
	            <h2 class="distrib"><span class="distribution">Tianjin Huilitong</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>Tianjin Huilitong Steel Tube Co., Ltd. is a professional manufacturer of ERW steel pipes, SSAW steel pipes and Square/Rectangular steel pipes. The factory is conveniently located in the North-ring Industrial area of Tianjin Jinghai Economic Developing Area, which is only 75kms from Xingang Port. Tianjin has a registered capital of 100 million RMB and covers an area of 100,000 square meters.</p>     
	            <p>Their products are strictly conducted according to API 5L ,API 5CT,EN10217,EN10219,GB/T9711,and other equivalent standards, and are widely used for the transmission of petroleum, natural gas, and SP, They can also be used for building construction, water conveying, steel structure, piping and other related industries.</p>
	            <ul class="links-list">
	                <li class="undown">Website: <a href="http://www.longshenghua.com/">www.longshenghua.com</a></li>
	            </ul>       
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="virgo">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/virgo.png"/>
	            <h2 class="distrib"><span class="distribution">Virgo Engineers</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">     
	            <p>Virgo Engineers Group has focused on the manufacture and sales of automated and manually operated quarter turn valves. With large investments in sophisticated manufacturing facilities in Italy, India, USA and Germany, producing high quality products, Virgo has become one of the leaders in the flow control industry. Over the years, Virgo has acquired prestigious international certifications and approvals from major oil companies globally.</p>
	            <p>Virgo offers Ball Valves from 1/2" to 60" size and in pressure classes ANSI 150 to 2500. The product line includes Floating Ball Valves, Trunnion Mounted Ball Valves, Metal Seated Ball Valves, Welded Body Ball Valves, Cryogenic Valves and Top Entry Valves in a wide choice of standard materials as well as special materials such as Duplex, Super Duplex and Inconel, etc.</p>
	            <ul class="links-list">
	                <li class="undown">Website: <a href="http://www.virgoengineers.com/">www.virgoengineers.com</a></li>
	            </ul>       
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="bf">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/bf.png"/>
	            <h2 class="distrib"><span class="distribution">Bonney Forge</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">     
	            <p>Bonney Forge produces the industry's widest range of forged steel valves. From stainless steel to carbon steel, common alloys to exotic alloys, we consistently use the highest caliber materials that meet the most stringent of industry codes and specifications.</p>
	            <p>The Bonney Forge line of Cast Steel Valves is extensive and complete. Choose from a full line of cast steel gate, globe and check valves. We also offer specialty cast steel valves: Y-patterns, cryogenics, bellow seal, globes and checks. We produce both standard and customized configurations-fast, and offer a complete choice of trim, body materials, and special features.</p>
	            <ul class="links-list">
	                <li class="undown">Website: <a href="http://www.bonneyforge.com/">www.bonneyforge.com</a></li>
	            </ul>       
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <!-- <div class="rows-content box separator" id="fabrotech">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/fabrotech.png"/>
	            <h2 class="distrib"><span class="distribution">Fabrotech</span></h2>            
	        </div> <!-- /row -->
	        <!-- <div class="row-text">     
	            <p>High quality check valves, y-strainers, basket strainers, duplex strainers, fabricated products, and other pipeline accessories for industrial applications in Cast Iron, Carbon Steel,Bronze and Stainless Steel.</p>
	            <ul class="links-list">
	                <li><a href="media/docs/fabrotech/FABROTECH_AUTH_LETTER_1-11-13.pdf">FABROTECH - TEKNACORP Authorization Letter</a></li>
	                <li class="undown">Website: <a href="http://www.fabrotech.com/">www.fabrotech.com</a></li>
	            </ul>       
	        </div> --> <!-- /row-text -->
	    <!-- </div> --> <!-- /rows-content -->
	    <div class="rows-content box separator" id="wfi">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/wfi.png"/>
	            <h2 class="distrib"><span class="distribution">WFI</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">     
	            <p>WFI, a division of Bonney Forge is a leading manufacturer of ferrous and non-ferrous branch connection fittings, specialty flanges, and seamless fittings for use in piping systems and on pressure vessels. WFI and Bonney Forge are the world's leading manufacturers of integrally reinforced branch connection fittings</p>
	            <ul class="links-list">
	                <li class="undown">Website: <a href="http://www.wfi-intl.com/">www.wfi-intl.com</a></li>
	            </ul>       
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <!-- <div class="rows-content box separator" id="isolatek">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/isolatek.png"/>
	            <h2><span class="distribution">Isolatek - Cafco</span></h2>            
	        </div> <!-- /row -->
	        <!-- <div class="row-text">            
	            <p>CAFCO BLAZE-SHIELD is the world's best selling Dry-Mix sprayed fire protection product. It has also made ISOLATEK the fastest growing manufacturer of passive fire resistive materials in the world, developing the broadest selection of passive fire protection products in the industry. From spray-applied fire resistive materials (SFRM), to fire stopping, thin film intumescent coatings and rigid board fire protection systems.</p>     
	            <p>Download from our page additional info from this prestigious brand:</p>
	            <ul class="links-list">
	                <li><a href="media/docs/isolatek/FendoliteSpanish.pdf">FENDOLITE</a></li>
	                <li><a href="media/docs/isolatek/Blaze-ShieldIIEspanol.pdf">Blaze Shield II</a></li>
	                <li><a href="media/docs/isolatek/CARTAISOLATEK.pdf">Representation Letter</a></li>
	            </ul>         
	        </div>  --><!-- /row-text -->
	    <!-- </div>  --><!-- /rows-content -->
	    <div class="rows-content box separator" id="sypris">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/sypris.png"/>
	            <h2 class="distrib"><span class="distribution">Sypris Technologies</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>Sypris Technologies</p>     
	            <p>Download from our page additional info from this prestigious brand:</p>
	            <ul class="links-list">
	                <li><a href="media/docs/sypris/Line_Card_TTT.pdf">SYPRIS - Line Catalog</a></li>
                	<!-- <li><a href="media/docs/sypris/CARTASYPRIS.pdf">Representation Letter</a></li> -->
                	<li class="undown">Website: <a href="http://www.sypristechnologies.com/">www.sypristechnologies.com</a></li>
	            </ul>          
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="aitken">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/aitken.png"/>
	            <h2 class="distrib"><span class="distribution">Aitken</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <p>Basket type Filters, "Y" type filters, Temporal filters, Conic filters, Permanent filters, "8" Figures, Venturis, ASME Recipients.</p>     
	            <p>Download from our page additional info from this prestigious brand:</p>
	            <ul class="links-list">
	                <li><a href="media/docs/aitken/aitken-catalog.pdf">AITKEN Catalog</a></li>
                	<!-- <li><a href="media/docs/aitken/CARTA.pdf">Representation Letter</a></li> -->
	            </ul>        
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <!-- <div class="rows-content box" id="nupigeco">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/nupigeco.png"/>
	            <h2><span class="distribution">Nupigeco Group SPA</span></h2>            
	        </div> <!-- /row -->
	        <!-- <div class="row-text">            
	            <p>GECO offer a complete range of pipes and fittings produced with the latest thermoplastic materials having brand names NIRON, MULTINUPI, POLIETILENE TUBI, ELOFIT, ELOPRESS, ELOTHERM, POLYSYSTEM, PVC FITTINGS.</p>
	            <p>These are real "solution systems" capable of solving any problem of installation, cost reduction, avoiding waste and increasing the efficiency of the installation workmanship.</p>
	            <p>GECO combines a high productivity with elevated and consistent quality standards. On these sound foundations lays the leadership in a field of strong competition and high technology such is that of plastic materials. A supremacy that Nupi holds by offering prime quality products and a customer oriented mentality.</p>     
	            <p>Download from our page additional info from this prestigious brand:</p>
	            <ul class="links-list">
	                <li><a href="media/docs/nupigeco/OILTECHCATALOGUSA.pdf">OILTECH - Product Catalogue</a></li>
	                <li><a href="media/docs/nupigeco/Smartflex.pdf">Smartflex</a></li>
	                <li><a href="media/docs/nupigeco/B-Collari-Saddles.pdf">Branch Saddles</a></li>
	                <li><a href="media/docs/nupigeco/Transitio_fitt.pdf">Transition Fittings</a></li>
	                <li><a href="media/docs/nupigeco/C-Valvole-Valves.pdf">Valves</a></li>
	                <li><a href="media/docs/nupigeco/EFFittings.pdf">Electrofusion Fittings</a></li>
	                <li><a href="media/docs/nupigeco/DVGWCertificate180-400mm.pdf">Certificate for DVGW test mark - 1</a></li>
	                <li><a href="media/docs/nupigeco/dvgwTestmark.pdf">Certificate for DVGW test mark - 2</a></li>
	                <li><a href="media/docs/nupigeco/DVGWCertificate.pdf">DVGW Type Examination Certificate - 1</a></li>
	                <li><a href="media/docs/nupigeco/DVGWCertificatesmallfittings.pdf">DVGW Type Examination Certificate - 2</a></li>
	                <li><a href="media/docs/nupigeco/IQNETCISQUNIENISO9001.pdf">IQNET Certificate ISO 9001</a></li>
	                <li><a href="media/docs/nupigeco/IQNETCISQUNIENISO14001.pdf">IQNET Certificate ISO 14001</a></li>
	                <li><a href="media/docs/nupigeco/IIP_UNI_EN_1555.pdf">Certificate IIP-UNI</a></li>
	                <li><a href="media/docs/nupigeco/CHINACERTIFICATES.pdf">Certificates CHINA</a></li>
	                <li><a href="media/docs/nupigeco/OVGW_AUSTRIA.pdf">Certificate OVGW AUSTRIA</a></li>
	                <li><a href="media/docs/nupigeco/OS_BULGARO.pdf">Certificate BULGARIAN</a></li>
	                <li><a href="media/docs/nupigeco/RegistroEspana.pdf">Registration SPAIN</a></li>
	                <li><a href="media/docs/nupigeco/CARTANUPIGEC.pdf">Representation Letter</a></li>
	            </ul>         
	        </div>  --><!-- /row-text -->
	    <!-- </div>  --><!-- /rows-content -->
	    <div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
    </div> <!-- /rows -->

    <!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->
</body>
</html>
