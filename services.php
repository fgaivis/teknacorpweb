<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="all,follow" />
    <meta name="author" lang="en" content="All: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="copyright" lang="en" content="Webdesign: Soluciones Dynamtek C.A. [www.dynamtek.com]; e-mail: info@dynamtek.com" />
    <meta name="description" content="Solutions for the Oil and Gas Industry" />
    <meta name="keywords" content="USA, Houston, Venezuela, Mexico, Solutions, Engineering, Mechanical, Capability, Carmona, Hoffmann" />
    
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />
    <!--[if lte IE 6]><link rel="stylesheet" media="screen,projection" type="text/css" href="css/main-ie6.css" /><![endif]-->
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/style.css" />
    <link rel="stylesheet" media="screen,projection" type="text/css" href="css/menu.css" />
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    
	<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.innerfade.js"></script>
	<script type='text/javascript' src="js/jquery.scrollTo-min.js"></script>
	<script type="text/javascript" src="js/jquery.localscroll-min.js"></script>
    <script type="text/javascript" src="js/jquery.simplemodal.js"></script>
    <script type="text/javascript" src="js/jssor.core.js"></script>
    <script type="text/javascript" src="js/jssor.utils.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script type="text/javascript">
    $(document).ready(
    function(){
        $('#slider').innerfade({
            animationtype: 'fade',
            speed: 750,
            timeout: 8000,
            type: 'sequence',
            containerheight: 'auto'
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
    	$.localScroll();
    });
    </script>
    <script type="text/javascript">
	$(document).ready(function(){		
		$("ul.topnav li a").hover(function() { //When trigger is clicked...
			//Following events are applied to the subnav itself (moving subnav up and down)
			$(this).parent().find("ul.subnav").slideDown('slow').show(); //Drop down the subnav on click
			
			$(this).parent().hover(function() {
			}, function(){
				$(this).parent().find("ul.subnav").slideUp('fast'); //When the mouse hovers out of the subnav, move it back up
			});
	
			//Following events are applied to the trigger (Hover events for the trigger)
			}).hover(function() {
				$(this).addClass("subhover"); //On hover over, add class "subhover"
			}, function(){	//On Hover Out
				$(this).removeClass("subhover"); //On hover out, remove class "subhover"
		});	
	});	
	</script>
    <title>Teknacorp - Services </title>
</head>

<body>
<div id="top">
	<!-- Logo -->
	<div id="logo"><a href="index.php" title="Teknacorp - Home Page"><img src="images/logo_fonts.png" alt="Teknacorp" /></a></div>
	<div id="promotions">
		<?php include 'promotions.php';?>
	</div>
</div>
<div id="main">
    <!-- Header -->
    <div id="header" class="box">
        <!-- Navigation -->
        <?php include 'navigation.php';?>
        
        <!-- Promo -->
    	<div id="promo">
            <!-- <p id="slogan"><img src="design/slogan.gif" alt="Place for your slogan" /></p> -->            
            <ul id="slider">
                <li><img src="images/p_services.jpg" alt="" /></li>
                <!-- <li><img src="images/promo2.jpg" alt="" /></li>  -->
            </ul>        
   	 	</div> <!-- /promo -->
    </div> <!-- /header -->
    
    <h1 id="title">Services</h1>
    
    <!-- Content Rows -->
    <div class="rows separation">    
        <div class="rows-content box separator" id="pe">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/projeng.jpg"/>
	            <h2><span class="products">Project Engineering</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">
	            <p>Our methodology is based on creating a close relationship with our customers of technical nature based on Communication, Trust and Cooperation. These are primary conditions for us to engage to your supply needs.</p>        
	            <p>We account with an experienced team in:</p>
	            <ul>
	                <li>ASME Calculations.</li>
	                <li>Project Management: Inspection, Expediting and Agilization.</li>
	                <li>Design of Pipeline Components.</li>
	            </ul>
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box separator" id="sol">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/solutions.jpg"/>
	            <h2><span class="products">Solutions</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	            <ul>
	                <li>Express Solutions to your Supply Needs. We can place any component in your Jobsite in 48 Hrs if necessary.</li>
	                <li>Project Solutions for your Long Lead Bulk or Short lead Requirements.</li>
	                <li>Logistic and Traffic Capability combining capacity with our Affiliate freight forwarders in USA and the Americas.</li>
	                <li><a href="products.php#pig-launchers">Modular Solutions</a></li>
	            </ul>
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="log">
	    	<div class="row" style="margin-right: 46px;">            
	            <img src="images/logistics.png"/>
	            <h2><span class="products">Logistics</span></h2>            
	        </div> <!-- /row -->
	        <div class="row-text">            
	           <p>Teknacorp Logistics capacity is the key to successfully culminating our commitment to your projects. We have in place distinguished resources for our transportation, warehousing and packaging solutions. Our outstanding Logistics processes guarantee our clients cost effectiveness and efficient transportation that provides secure packaging complying with international regulations and customer requirements.</p>
	           <p>Teknacorp has developed partnerships with transportation services companies to cover all the demanded delivery activities of our market in all Latin America and the Caribbean, including material packaging, handling, transportation and insurances operating form the main ports of from the US, Europe and Asia guaranteeing high product quality and delivery in the best time possible. We partner with all our customers incorporating their specific needs to come up with comprehensive solutions that fit their requirements for every single project.</p>
	           <p>In Teknacorp we understand the importance of material handling in a safe, fast and efficient manner to achieve or improve our client’s expectations regarding packaging, handling and transportation of materials. Our ability to integrate leading products delivers greater certainty, reliability and peace of mind.</p>            
	           <br/><br/>
	           <!--  
	           <ul>
	                <li>Project Engineering</li>
	                <li>Technical Service</li>
	                <li>Professional Assesment</li>
	                <li>Procurement</li>
	                <li>Import, Export and Customs</li>
	                <li>Added value</li>
	                <li>Training</li>
	                <li>Storage</li>
	                <li>Inventory Management </li>
	            </ul> 
	            -->
	        </div> <!-- /row-text -->
	    </div> <!-- /rows-content -->
	    <div class="rows-content box" id="backtop" style="text-align: right;">
	    	<a href="#top">&uarr; Back to top</a>
	    </div>
    </div> <!-- /rows -->

    <!-- Footer -->
    <?php include 'footer.php';?>

</div> <!-- /main -->
</body>
</html>
